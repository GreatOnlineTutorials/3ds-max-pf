#!/bin/bash
set -e

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_Birth Grid 出生栅格 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_Birth Group 出生组 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_Birth Stream 出生流 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="04_PhysX_Drag 阻力 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="05_PhysX_Force 力 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc05.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc05.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="06_PhysX_Shape 碰撞外形 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc06.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc06.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="07_PhysX_Switch 开关 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc07.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc07.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="08_PhysX_Collision 碰撞 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc08.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc08.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="09_PhysX_FaceCreator 面生成 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc09.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc09.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="10_PhysX_Inter_Collision 内部碰撞 [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc10.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc10.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="11_PhysX_Solven 绑定解除 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc11.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc11.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="12_PhysX_World 引擎 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc12.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc12.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="13_preset 预设 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc13.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc13.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="14_01_粒子和破碎01 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc14_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc14_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="14_02_粒子和破碎02 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc14_02.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc14_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="15_01_打碎多物体01 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc15_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc15_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="15_02_打碎多物体02 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc15_02.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc15_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="16_fumefx&box2 流体和粒子 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc16.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc16.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="17_Cloth 布料 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc17.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc17.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="18_开关和改变外形 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc18.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc18.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="19_mass_random 质量随机 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc19.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc19.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="20_Softbody&Rigidbody 刚体和柔体 [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc20.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc20.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="21_Dropdown 音符掉落 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc21.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc21.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="22_01_渐现渐落01 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc22_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc22_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="22_02_渐现渐落02 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc22_02.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc22_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="22_03_渐现渐落03 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc22_03.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc22_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="23_breakbuilding 大楼局部碎裂 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc23.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc23.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="24_box3_距离控制缩放 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc24.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc24.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="25_box3_顶点颜色 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc25.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc25.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="26_box3_多对象控制顶点颜色 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc26.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc26.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="27_box3_噪波控制XY向缩放 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc27.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc27.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="28_box3_点数量和距离 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc28.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc28.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="29_box3_随机和位置 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc29.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc29.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="30_01_物体内部01 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc30_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc30_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="30_02_物体内部02 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc30_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc30_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="31_box3_距离&可见性 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc31.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc31.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="32_box3_保存预设 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc32.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc32.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="33_box3_缩放&位置&噪波贴图 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc33.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc33.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="34_box3_跟随物体 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc34.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc34.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="35_box3_多条件控制 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc35.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc35.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="36_01_自旋速率&自旋轴&缩放01 [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc36_01.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc36_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="36_02_自旋速率&自旋轴&缩放02 [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc36_02.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc36_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="36_03_自旋速率&自旋轴&缩放03 [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc36_03.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc36_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="37_box3_物体控制粒子朝向 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc37.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc37.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="38_box3_随机子控制器 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc38.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc38.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍 - 第1篇 PF粒子全系列介绍\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="39_box3_绕物体飞行 [第1篇 PF粒子全系列介绍] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/jc39.wmv" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/jc39.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_腾空而爆 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_怦然下落01 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps02_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps02_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_怦然下落02 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps02_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps02_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_时间静止01 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps03_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps03_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_时间静止02 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps03_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps03_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_时间静止03 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps03_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps03_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_辉飞狮灭01 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps04_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps04_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_辉飞狮灭02 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps04_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps04_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_辉飞狮灭03 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps04_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps04_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_人体爆碎01 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps05_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps05_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第2篇 破碎特效 - 第2篇 破碎特效 - 第2篇 破碎特效\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_人体爆碎02 [第2篇 破碎特效] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD1/right/video/ps05_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD1/right/ps05_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_魔幻黑豆01 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl01_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl01_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_魔幻黑豆02 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl01_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl01_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_靓丽珍珠01 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl02_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl02_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_靓丽珍珠02 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl02_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl02_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_时光飞逝01 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl03_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl03_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_时光飞逝02 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl03_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl03_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_时光飞逝03 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl03_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl03_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_如影随形01 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl04_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl04_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_如影随形02 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl04_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl04_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_如影随形03 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl04_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl04_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_羽球快递 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl05.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl05.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_M豆争艳01 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl06_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl06_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_M豆争艳02 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl06_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl06_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第3篇 动力学运算 - 第3篇 动力学运算 - 第3篇 动力学运算\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_M豆争艳03 [第3篇 动力学运算] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/dl06_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/dl06_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_金刚世界 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_缤纷字母 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_果飞蛋打01 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz03_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz03_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_果飞蛋打02 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz03_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz03_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_果飞蛋打03 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz03_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz03_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_粉色世界01 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz04_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz04_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_粉色世界02 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz04_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz04_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_海纳百川01 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz05_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz05_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_海纳百川02 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz05_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz05_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第4篇 文字演绎 - 第4篇 文字演绎 - 第4篇 文字演绎\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="03_海纳百川03 [第4篇 文字演绎] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/wz05_03.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/wz05_03.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第5篇 光效渲染 - 第5篇 光效渲染 - 第5篇 光效渲染\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_波浪起伏 [第5篇 光效渲染] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/gx01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/gx01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第5篇 光效渲染 - 第5篇 光效渲染 - 第5篇 光效渲染\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_绚丽夺目01 [第5篇 光效渲染] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/gx02_01.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/gx02_01.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第5篇 光效渲染 - 第5篇 光效渲染 - 第5篇 光效渲染\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="02_绚丽夺目02 [第5篇 光效渲染] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/gx02_02.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/gx02_02.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

(printf "《3ds Max影视特效火星课堂——PF高级案例篇》\n第5篇 光效渲染 - 第5篇 光效渲染 - 第5篇 光效渲染\n本书采用循序渐进的方式，讲解了3ds Max的PF粒子系统。图书采用案例的形式进行讲解，案例全部为影视特效行业的高级应用案例。效果精美，制作思路清晰，让读者可以轻松简单的，就掌握PF的应用技巧。\n\nhttp://got.notsh.it/3ds-max-pf" | youtube-upload --email=guesswhatihate@gmail.com --password= --category=Education --keywords=3ds --title="01_焦散小球 [第5篇 光效渲染] [3ds Max PF高级案例]" --description="$(< /dev/stdin)" "DVD2/right/video/gx04.avi" | sed 's/.*\([-A-Za-z0-9\_]\{11\}\).*/\1/' - | tr -d "\n" && printf " => DVD2/right/gx04.html\n") >> ./log && tail -n 1 ./log;

sleep 5;

(printf "http://www.youtube.com/watch?v=" && tail -n 1 ./log | cut -c 1-11) | youtube-upload --email=guesswhatihate@gmail.com --password= --add-to-playlist=http://gdata.youtube.com/feeds/api/playlists/CJcQMZOafIBhqFCzIzAaIYiCbAoOF8eZ $(< /dev/stdin)

sleep 5;

